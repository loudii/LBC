import { Component, OnInit } from '@angular/core';
import { AdService } from '../services/ad.service';
import {Status} from '../status';

@Component({
  selector: 'app-ads-view',
  templateUrl: './ads-view.component.html',
  styleUrls: ['./ads-view.component.scss']
})
export class AdsViewComponent implements OnInit {

  ads: any[];
  public isAuthenticated:boolean = true;

  constructor(private adService: AdService) { }

  ngOnInit() {
    this.ads = this.adService.ads;
  }

  onPublish() {
    this.ads.forEach(ad => ad.status = Status.Published)
  }

  login() {
    this.isAuthenticated = true;
  }

  logout() {
    this.isAuthenticated = false;
  }

}
