import { Component, OnInit } from '@angular/core';
import { Status } from './status';
import { AdService } from './services/ad.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  isAuth = false;

  private ads: any[] = []


  constructor(private adService: AdService) {

      setTimeout(
        () => {
          this.isAuth = true;
        }
      );
    }


  ngOnInit() {
    this.ads = this.adService.ads;
  }
}
