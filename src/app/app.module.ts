import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { AdComponent } from './ad/ad.component';
import { AdService } from './services/ad.service';
import { CounterComponent } from './counter/counter.component';
import { LoginComponent } from './login/login.component';
import { AdsViewComponent } from './ads-view/ads-view.component';
import {RouterModule, Routes} from '@angular/router';
import { SingleAdComponent } from './single-ad/single-ad.component';
import { NewAdComponent } from './new-ad/new-ad.component';


const appRoutes: Routes = [
  { path: 'ads', component: AdsViewComponent },
  { path: 'login', component: LoginComponent },
  { path: 'ads/:id', component: SingleAdComponent },
  { path: 'new', component: NewAdComponent },
  { path: '', component: AdsViewComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    AdComponent,
    CounterComponent,
    LoginComponent,
    AdsViewComponent,
    SingleAdComponent,
    NewAdComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AdService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
