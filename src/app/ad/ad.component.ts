import { Component, OnInit, Input } from '@angular/core';
import { Status } from '../status'
import { AdService } from '../services/ad.service';

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.scss']
})
export class AdComponent implements OnInit {

  // propriétés
  @Input() public title: string = `ceci est une annonce`
  @Input() public status: string = Status.Published
  @Input() public date: Date
  @Input() public price: number
  @Input() public index: number
  @Input() id: number;

  public statuses = Status; // pour pouvoir accéder aux différents status dans le template

  constructor(private adService: AdService) { }

  ngOnInit() {
  }

  getColor() {
    let color = 'grey'
    color = this.status === Status.Published ? 'green' : color
    color = this.status === Status.Offline ? 'red' : color
    return color;
  }

  setStatus(status) {
    this.adService.ads[this.index].status = status
  }

}
