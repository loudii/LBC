import { Component , OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {AdService} from '../services/ad.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-ad',
  templateUrl: './new-ad.component.html',
  styleUrls: ['./new-ad.component.scss']
})
export class NewAdComponent implements OnInit {

  constructor(private adService: AdService,
              private router: Router) { }

  ngOnInit() {
  }
  onSubmit(form: NgForm) {
    const title = form.value['title'];
    const price = form.value['price'];
    const image = form.value['image'];
    const status = form.value['status'];
    this.adService.addAd(title, price, image, status);
    this.router.navigate(['/ads']);

  }
}
