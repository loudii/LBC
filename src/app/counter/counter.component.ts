import { Component, OnInit, OnChanges, DoCheck, Input } from '@angular/core';
import { AdService } from '../services/ad.service';
import { Status } from '../status'

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit, DoCheck {

  @Input() ads: any[]
  private count:number = 0

  constructor(private adService:AdService) { }

  ngOnInit() {
    this.ads = this.adService.ads
  }

  ngDoCheck() {
    this.count = this.ads.filter((ad) => ad.status == Status.Published).length
  }

}
