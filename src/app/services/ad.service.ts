import { Injectable } from '@angular/core';
import { Status } from '../status';

@Injectable()
export class AdService {

  public ads:any[] = [
    { id: 1, title: 'Moto à 6 roues', status: Status.Draft, date: new Date(), price: 1.333333 },
    { id: 2, title: 'Chien à 3 têtes', status: Status.Published, date: new Date(), price: 200 },
    { id: 3, title: 'Valse à 1000 temps', status: Status.Offline, date: new Date(), price: 10.5 },
  ]

  constructor() { }

  getAdsById(id: number) {
    const ad = this.ads.find(
      (s) => {
        return s.id === id;
      }
    );
    return ad;
  }

  addAd(title: string, price: number, image: string, status: string) {
    const adObject = {
                      id: 0,
                      title: '',
                      date: new Date(),
                      price: 0,
                      image: '',
                      status: ''
                    };
    adObject.title = title;
    adObject.price = price;
    adObject.image = image;
    adObject.status = status;
    adObject.id = this.ads[(this.ads.length - 1)].id + 1;
    this.ads.push(adObject);
  }
}
