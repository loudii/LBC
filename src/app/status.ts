export enum Status {
  Draft = 'Brouillon',
  Published = 'Publié',
  Offline = 'Désactivée',
}