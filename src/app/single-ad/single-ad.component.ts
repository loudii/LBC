import { Component, OnInit } from '@angular/core';
import { AdService } from '../services/ad.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-single-ad',
  templateUrl: './single-ad.component.html',
  styleUrls: ['./single-ad.component.scss']
})
export class SingleAdComponent implements OnInit {

  name: string = 'ads';
  status: string = 'Status';

  constructor(private adService: AdService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.name = this.adService.getAdsById(+id).title;
    this.status = this.adService.getAdsById(+id).status;
  }

}
